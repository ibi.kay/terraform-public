The following Terraform code will create and attach a Transit Gateway (TGW) for the given AWS account.

You will need to create environment variables for the following;

AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_SESSION_TOKEN (if you're assuming a role in a target account)

When attaching a TGW to a VPC, you can only specify one subnet from each Availability Zone. This code will scan
for all VPC's in the account (based on the access/secret key) with the specified tags and iteratively return the subnet_ids.

If there are multiple subnets in the same Availability Zone, this code will fail with a
'DuplicateSubnetsInZone' error, as it tries to attach all subnets in all VPC's to the TGW. To get around this,
use tags to only attach to private subnets.
