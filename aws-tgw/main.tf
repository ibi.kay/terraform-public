# Fetch the VPC ID's for all production VPC's with a tag of `service = production`
data "aws_vpcs" "vpcs" {
  tags = {
    service = "production"
  }
}

# Fetch the subnet ID's for the select VPC with a tag of 'tier = private'
data "aws_subnet_ids" "subnets" {
  for_each      = data.aws_vpcs.vpcs.ids
  vpc_id        = each.value
  tags = {
    tier = "private"
  }
}

# Create the TGW..
resource "aws_ec2_transit_gateway" "tgw" {
  description                     = var.tgw_description
  amazon_side_asn                 = var.tgw_amazon_asn
  auto_accept_shared_attachments  = "disable"
  default_route_table_association = "enable"
  default_route_table_propagation = "enable"
  dns_support                     = "enable"
  vpn_ecmp_support                = "enable"

  tags = {
    name = var.tgw_name
  }
}

# Attach to VPC and subnets.
resource "aws_ec2_transit_gateway_vpc_attachment" "attach_tgw_vpc" {
  for_each                      = data.aws_subnet_ids.subnets
  transit_gateway_id            = aws_ec2_transit_gateway.tgw.id
  subnet_ids                    = each.value.ids
  vpc_id                        = each.value.id
}
