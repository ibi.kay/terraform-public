variable "aws_region" {
  type        = string
  description = "aws region to use"
}

variable "tgw_name" {
  type        = string
  description = "TGW name"
}

variable "tgw_description" {
  type        = string
  description = "TGW description"
}

variable "tgw_amazon_asn" {
  type        = number
  description = "Amazon side ASN"
}
